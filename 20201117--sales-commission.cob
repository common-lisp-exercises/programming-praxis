       IDENTIFICATION DIVISION.
      *************************
       PROGRAM-ID.          SALES-COMMISSION.
       AUTHOR.              BOURGUIGNON PASCAL.
       DATE-WRITTEN.        NOVEMBER 2020.
       DATE-COMPILED.
      *
      *
      *
      *
       ENVIRONMENT DIVISION.
      **********************
       CONFIGURATION SECTION.
      *
       SOURCE-COMPUTER.     LINUX.
       OBJECT-COMPUTER.     LINUX.
       SPECIAL-NAMES.
           DECIMAL-POINT IS POINT.
      *
      *
       INPUT-OUTPUT SECTION.
      *
       FILE-CONTROL.
           SELECT FINPUT  ASSIGN TO 'FINPUT'.
           SELECT FIPRINT ASSIGN TO 'FIPRINT'.
      *
      *
      *
      *
       DATA DIVISION.
      ***************
       FILE SECTION.
      *
       FD  FINPUT
           RECORDING MODE F
           RECORD 73
           BLOCK 0
           LABEL RECORD OMITTED
           DATA RECORD SALES-E.
       01  SALES-E.
           02  EMPLOYEE-NAME-E    PIC X(60).
           02  SALES-AMOUNT-E     PIC 999999999V99.
           02  FILLER             PIC X(1).
       FD  FIPRINT
           RECORDING MODE F
           RECORD 133
           BLOCK 0
           LINAGE IS 66 LINES
           LABEL RECORD OMITTED
           DATA RECORD LIGNE-S.
       01  LIGNE-S                PIC X(133).
      *
       WORKING-STORAGE SECTION.
      *
       77  CTRVEND                PIC 9(5)       VALUE ZERO.
       77  FF                     PIC X          VALUE LOW-VALUE.
       77  PRIM4                  PIC 999V99     VALUE ZERO.
       77  SOMME                  PIC 9(5)V99.
       77  TOTSOC                 PIC 9999V99    VALUE 0.
       77  MOYPRIM                PIC 999V99     VALUE 0.
       77  TOTVEND                PIC 9999V99    VALUE ZERO.
       77  FICARD-NAME            PIC X(100).
       77  FIPRINT-NAME          PIC X(100).
       01  TITRE1.
           02  FILLER             PIC X(38)      VALUE SPACES.
           02  FILLER             PIC X(50)      VALUE ALL '*'.
           02  FILLER             PIC X(45)      VALUE SPACES.
       01  TITRE2.
           02  FILLER             PIC X(38)      VALUE SPACES.
           02  FILLER             PIC X          VALUE '*'.
           02  FILLER             PIC X(48)      VALUE SPACES.
           02  FILLER             PIC X          VALUE '*'.
           02  FILLER             PIC X(45)      VALUE SPACES.
       01  TITRE3.
           02  FILLER              PIC X(38) VALUE SPACES.
           02  FILLER              PIC X(25)
                             VALUE '*  A T T R I B U T I O N'.
           02  FILLER              PIC X(25)
                             VALUE '  D E S   P R I M E S   *'.
           02  FILLER              PIC X(45)     VALUE SPACES.
       01  TITRE4.
           02  T41                 PIC BBBX(7)BBX(7).
           02  T42                 PIC B(5)XXXB(23)X(6).
           02  T43                 PIC B(14)X(7)B(5)X(7)B(5).
           02  T44                 PIC X(7)B(5)X(7)BBBBX(12)BB.
      *
       01  VENDEUR-S.
           02  FILLER              PIC X(6)      VALUE SPACES.
           02  MAGASIN-S           PIC 99        VALUE 0.
           02  FILLER              PIC X(6)      VALUE SPACES.
           02  NOVEND-S            PIC 9999      VALUE 0.
           02  FILLER              PIC X(6)      VALUE SPACES.
           02  NOM-S               PIC X(20)     VALUE SPACES.
           02  FILLER              PIC X(6)      VALUE SPACES.
           02  PRENOM-S            PIC X(15)     VALUE SPACES.
           02  FILLER              PIC X(6)      VALUE SPACES.
           02  PRIM1-S             PIC ZZ9,99.
           02  FILLER              PIC X(6)      VALUE SPACES.
           02  PRIM2-S             PIC ZZ9,99.
           02  FILLER              PIC X(6)      VALUE SPACES.
           02  PRIM3-S             PIC ZZ9,99.
           02  FILLER              PIC X(6)      VALUE SPACES.
           02  PRIM4-S             PIC ZZ9,99.
           02  FILLER              PIC X(6)      VALUE SPACES.
           02  TOTVEND-S           PIC ZZZ9,99.
           02  FILLER              PIC X(6)      VALUE SPACES.
       01  TOTAL.
           02  FILLER              PIC X(87)     VALUE SPACES.
           02  FILLER              PIC X(19)
                             VALUE 'TOTAL DES PRIMES 4:'.
           02  TOTSOC-S            PIC ZZZZ9,99.
           02  FILLER              PIC X(19)     VALUE SPACES.
       01  MOYENNE.
           02  FILLER              PIC X(85)     VALUE SPACES.
           02  FILLER              PIC X(21)
                             VALUE 'MOYENNE DES PRIMES 4:'.
           02  MOYPRIM-S           PIC ZZZZ9,99.
           02  FILLER              PIC X(19)     VALUE SPACES.
      *
      *
      *
      *
      *
      *
       PROCEDURE DIVISION.
      ********************
           OPEN INPUT FICARD OUTPUT FIPRINT.
      *
       10.
      *    WRITE LIGNE-S FROM TITRE1 AFTER SAUT.
           WRITE LIGNE-S FROM TITRE1.
           WRITE LIGNE-S FROM TITRE2.
           WRITE LIGNE-S FROM TITRE3.
           WRITE LIGNE-S FROM TITRE2.
           WRITE LIGNE-S FROM TITRE1.
           MOVE 'MAGASINVENDEUR' TO T41.
           MOVE 'NOMPRENOM' TO T42.
           MOVE 'PRIME 1PRIME 2' TO T43.
           MOVE 'PRIME 3PRIME 4TOTAL PRIMES' TO T44.
           WRITE LIGNE-S FROM TITRE4 AFTER 5 LINES.
           MOVE ALL '*' TO T41 T42 T43 T44.
           WRITE LIGNE-S FROM TITRE4.
           MOVE SPACES TO TITRE4.
           WRITE LIGNE-S FROM TITRE4 AFTER 2 LINES.
           READ FICARD AT END MOVE HIGH-VALUE TO FF END-READ.
       20.
           ADD MONT1-E MONT2-E MONT3-E GIVING SOMME.
           MOVE MAGASIN-E TO MAGASIN-S.
           MOVE NOVEND-E TO NOVEND-S.
           MOVE NOM-E TO NOM-S.
           MOVE PRENOM-E TO PRENOM-S.
           MOVE PRIM1-E TO PRIM1-S.
           MOVE PRIM2-E TO PRIM2-S.
           MOVE PRIM3-E TO PRIM3-S.
           IF SOMME < 18000 GO TO 70 END-IF.
       30.
           IF SOMME < 25000 GO TO 50 END-IF.
       40.
           MOVE 500,00 TO PRIM4.
           GO TO 60.
       50.
           MOVE 400,00 TO PRIM4.
       60.
           GO TO 110.
       70.
           IF SOMME < 10000 GO TO 90 END-IF.
       80.
           MOVE 250,00 TO PRIM4.
           GO TO 100.
       90.
           MOVE 0 TO PRIM4.
       100.
       110.
           ADD PRIM1-E PRIM2-E PRIM3-E PRIM4 GIVING TOTVEND.
           ADD 1 TO CTRVEND.
           ADD PRIM4 TO TOTSOC.
           MOVE PRIM4 TO PRIM4-S.
           MOVE TOTVEND TO TOTVEND-S.
           WRITE LIGNE-S FROM VENDEUR-S.
           READ FICARD AT END MOVE HIGH-VALUE TO FF END-READ.
           IF NOT FF = HIGH-VALUE GO TO 20 END-IF.
       120.
           DIVIDE TOTSOC BY CTRVEND GIVING MOYPRIM.
           MOVE TOTSOC TO TOTSOC-S.
           WRITE LIGNE-S FROM TOTAL AFTER 3 LINES.
           MOVE MOYPRIM TO MOYPRIM-S.
           WRITE LIGNE-S FROM MOYENNE AFTER 2 LINES.
           CLOSE FICARD FIPRINT.
           GOBACK.
