;; https://programmingpraxis.com/2021/06/15/cardinal-and-ordinal-numbers/#respond
;;
;; Cardinal And Ordinal Numbers
;; June 15, 2021
;;
;; Cardinal numbers are those used for counting; spelled in letters, they
;; are one, two, three, four, and so on.
;;
;; Ordinal numbers are those used for ranking: spelled in letters, they
;; are first, second, third, fourth, and so on.
;;
;; Your task is to write a program that takes a number and returns the
;; spelled-out cardinal and ordinal forms of that number. When you are
;; finished, you are welcome to read or run a suggested solution, or to
;; post your own solution or discuss the exercise in the comments below.


(defun card-ord (n)
  (values (format nil "~R" n)
          (format nil "~:R" n)))

(assert (equal (multiple-value-list (card-ord 42))
               '("forty-two"
                 "forty-second")))

;; Done.


(defun main ()
  (loop for i below 111
        do (multiple-value-bind (card ord) (card-ord i)
             (format t "~A is ~A, the ~A number after 0.~%" i card ord))))

(main)
