#|
https://programmingpraxis.com/2021/04/27/remove-multiples/

Remove Multiples
April 27, 2021
Today’s exercise comes from Stack Overflow:

Given a number N and a set of numbers s = {s1, s2, …, sn} where s1 <
s2 < … < sn < N, remove all multiples of {s1, s2, …, sn} from the
range 1..N.

You should look at the original post on Stack Overflow. The poster
gets the answer wrong (he excludes 3), he get the explanation of his
wrong answer wrong (he excludes 10 as a multiple of 2), and his
algorithm is odd, to say the least. I’m not making fun of him, but I
see this kind of imprecision of thought all the time on the
beginner-programmer discussion boards, and I can only think that he
will struggle to have a successful programming career.

Your task is to write a program that removes multiples from a range,
as described above. When you are finished, you are welcome to read or
run a suggested solution, or to post your own solution or discuss the
exercise in the comments below.

|#


;; This looks like eratostene sieve

(eval-when (:compile-toplevel :load-toplevel :execute)

  (defgeneric sortedp (sequence)
    (:method ((list list))
      (loop
        :for n :in list
        :for m :in (rest list)
        :always (<= n m)))
    (:method ((vector vector))
      (loop
        :for i :below (1- (length vector))
        :always (<= (aref vector i) (aref vector (1+ i))))))

  (assert (every (function sortedp)
                 (list #()
                       #(1)
                       #(1 2)
                       #(1 2 3)
                       #(1 2 3 4)
                       #(3 3 3 3)
                       '()
                       '(1)
                       '(1 2)
                       '(1 2 3)
                       '(1 2 3 4)
                       '(3 3 3 3))))

  (assert (notany (function sortedp)
                  (list #(2 1)
                        #(1 3 2 4)
                        '(2 1)
                        '(1 3 2 4))))
  );; eval-when

(deftype sorted-sequence () `(and sequence (satisfies sortedp)))

(defun remove-multiples (up-to factors)
  (check-type up-to (integer 1))
  (check-type factors list)
  (check-type factors sorted-sequence)
  (let ((numbers (make-array (1+ up-to) :element-type 'bit :initial-element 1)))
    (dolist (factor factors)
      (unless (zerop (aref numbers factor)) ; avoid useless work
        (loop
          :for n :from factor :to up-to :by factor
          :do (setf (aref numbers n) 0))))
    (loop
      :for i :from 1 :to up-to
      :when (plusp (aref numbers i))
        :collect i)))

(remove-multiples 100 '(2 3 5 7 11 13 19))
;; --> (1 17 23 29 31 37 41 43 47 53 59 61 67 71 73 79 83 89 97)

