
;; https://programmingpraxis.com/2020/11/17/sales-commission/
;; 
;; Write a program that inputs each employee’s sales, then computes and
;; prints the employee’s commission according to the formula $200 plus
;; 10% of sales. Stop the input using a sentinel value. At the end of the
;; input, display a one-dimensional array containing all the commission
;; amounts, and the total amount of commission paid. You may not use a
;; switch statement or multiple if statements.


;; Input file format:
;;   Fixed size record:
;;      employee name 60 chars
;;      sales amount  12 chars
;;   Sentinel record: employee name = "END"
;;
;; Compute:
;;    Commission = $200 + 10% of sales amount
;;
;; Output format:
;;    Employee name |       Commission
;;    --------------|-----------------
;;    Total:        | Total Commission


(defstruct (commission-entry
            (:type list)
            (:conc-name nil))
  employee-name
  sales-amount
  commission)


(defvar *commission-rate* 10/100)
(defun compute-commission (entry)
  (setf (commission entry)
        (+ 200.00 (* *commission-rate* (sales-amount entry)))))



(define-condition invalid-sales (parse-error)
  ((field :initarg :field :reader invalid-sales-field)
   (employee-name :initarg :employee-name :reader invalid-sales-employee-name))
  (:report (lambda (condition stream)
             (format stream "Invalid sales field: ~S for employee ~S"
                     (invalid-sales-field condition)
                     (invalid-sales-employee-name condition)))))

(define-condition invalid-record (file-error)
  ((record :initarg :record :reader invalid-record-record)
   (reason :initarg :reason :reader invalid-record-reason))
  (:report (lambda (condition stream)
             (format stream "Invalid record: ~S ~%because ~A"
                     (invalid-record-record condition)
                     (invalid-record-reason condition)))))

(defun read-employee-sales (stream)
  "
Reads fixed length records, separated by a newline:
employee name:  60 chars (space padded)
sales amount:   12 chars
Reading ends when the name is 'END'.
Return a list of (employee-name sales-amount).
"
  (loop
    :with result := '()
    :for record := (read-line stream nil nil)
    :while record
    :do (if (<= 72 (length record))
            (let ((name (string-trim " " (subseq record 0 60)))
                  (sales (ignore-errors
                          (let ((*read-eval* nil)
                                (*read-default-float-format* 'double-float))
                            (read-from-string (string-trim " " (subseq record 60 72)))))))
              (when (string= "END" name)
                (loop-finish))
              (if (realp sales)
                  (push (make-commission-entry :employee-name name
                                               :sales-amount sales)
                        result)
                  (cerror "Ignore the record"
                          'invalid-sales :field (subseq record 60 72)
                                         :employee-name name)))
            (cerror "Ignore the record"
                    'invalid-record :record record
                                    :reason "record is too small"))
    :finally (return (nreverse result))))

(defun test/read-employee-sales ()
  (assert
   (equal
    '(("John Wayne" 123456.89d0 nil)
      ("Chuck Norris" 999910.50d0 nil)
      ("Bruce Willis" 1504242.33d0 nil))
    (with-input-from-string (input "
John Wayne                                                  000123456.89
Chuck Norris                                                   999910.50
Bruce Willis                                                001504242.33
END                                                         000000000.00
Bugs Bunny                                                          1.00
")
      (handler-bind
          ((invalid-record (lambda (condition)
                             (declare (ignore condition))
                             (invoke-restart 'continue))))
        (read-employee-sales input)))))
  :success)



(defun read-employee-sales-file (pathname)
  (with-open-file (input pathname)
    (read-employee-sales input)))



(defun print-commissions (entries)
  (let ((total (reduce (function +) entries
                       :key (function commission)
                       :initial-value 0.0d0))
        (ftitle "~60A | ~12A~%")
        (fentry "~60A | ~12,2F~%")
        (fline  (format nil "~60@{-~}-|-~12@{-~}~%" nil)))
    (format t ftitle "Employee Name" "Commission")
    (write-string fline)
    (dolist (entry entries)
      (format t fentry (employee-name entry) (commission entry)))
    (write-string fline)
    (format t fentry "Total:" total)
    (values)))

(defun test/print-commissions ()
 (assert
  (equal
   (with-output-to-string (*standard-output*)
     (print-commissions (mapc (function compute-commission)
                              (list (make-commission-entry :employee-name "John Wayne"
                                                           :sales-amount 123456.89d0)
                                    (make-commission-entry :employee-name "Chuck Norris"
                                                           :sales-amount 999910.50d0)
                                    (make-commission-entry :employee-name "Bruce Willis"
                                                           :sales-amount 1504242.33d0)))))
   "Employee Name                                                | Commission  
-------------------------------------------------------------|-------------
John Wayne                                                   |     12545.69
Chuck Norris                                                 |    100191.05
Bruce Willis                                                 |    150624.23
-------------------------------------------------------------|-------------
Total:                                                       |    263360.97
"))
  :success)

(defun test/all ()
  (test/read-employee-sales)
  (test/print-commissions))

(test/all)

(defun main (pathname &rest arguments)
  (declare (ignore arguments))
  (print-commissions
   (mapc (function compute-commission)
         (read-employee-sales-file pathname)))
  (values))

;; (main #P"~/src/public/common-lisp-exercises/programming-praxis/20201117--sales-commission.input")
;; Employee Name                                                | Commission  
;; -------------------------------------------------------------|-------------
;; John Wayne                                                   |     12545.69
;; Chuck Norris                                                 |    100191.05
;; Bruce Willis                                                 |    150624.23
;; Michel Serrault                                              |       200.10
;; Michel Bouquet                                               |       300.00
;; Michel Blanc                                                 |  10000200.00
;; -------------------------------------------------------------|-------------
;; Total:                                                       |  10264061.07

