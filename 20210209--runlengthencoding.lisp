;; https://programmingpraxis.com/2021/02/09/run-length-encoding-2/

;; Run Length Encoding
;; February 9, 2021
;; Alexey Grigorev says:
;; 
;; Most candidates cannot solve this interview problem:
;; 
;; Input: “aaaabbbcca”
;; 
;; Output: [(“a”,4), (“b”,3), (“c”,2), (“a”,1)]
;; 
;; Write a function that converts the input to the output. I ask it in
;; the screening inverview and give it 25 minutes. How would you solve
;; it?


(defun run-length-encode (vector)
  (loop
    :with encoding := '()
    :with current-element := nil
    :with current-count := 0
    :for element :across vector
    :do (cond
          ((zerop current-count)
           (setf current-element element
                 current-count   1))
          ((equal current-element element)
           (incf current-count))
          (t
           (push (cons current-element current-count) encoding)
           (setf current-element element
                 current-count   1)))
    :finally (when (plusp current-count)
               (push (cons current-element current-count) encoding))
             (return (nreverse encoding))))

(assert (equal (run-length-encode "aaaabbbcca")
               '((#\a . 4) (#\b . 3) (#\c . 2) (#\a . 1))))

(defun run-length-decode (result-type runs)
  (let* ((length (reduce (function +) runs :key (function cdr)))
         (decoded (ecase result-type
                    ((base-string) (make-string length :element-type 'base-char))
                    ((string)      (make-string length :element-type 'character))
                    ((vector)      (make-array length)))))
    (loop
      :with i := -1
      :for (element . count) :in runs
      :do (loop :repeat count :do (setf (aref decoded (incf i)) element)))
    decoded))

(assert (equal (run-length-decode 'string (run-length-encode "aaaabbbcca"))
               "aaaabbbcca"))

(assert (equalp (run-length-decode 'vector (run-length-encode "aaaabbbcca"))
                #(#\a #\a #\a #\a #\b #\b #\b #\c #\c #\a)))


