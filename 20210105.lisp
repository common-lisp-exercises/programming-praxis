;; https://programmingpraxis.com/2021/01/05/two-simple-tasks/
;; 
;; Two Simple Tasks
;; January 5, 2021
;; Happy New Year! May 2021 be a better year than 2020.
;; 
;; We have two simple tasks today:
;; 
;; First: Write a program that prints the sequence 1, 5, 10, 50, 100, …
;; up to a given limit.
;; 
;; Second: Write a program that finds all three-digit numbers n such that
;; n/11 equals the sum of the squares of the three digits.
;; 
;; Your task is to write programs that solve the two tasks given
;; above. When you are finished, you are welcome to read or run a
;; suggested solution, or to post your own solution or discuss the
;; exercise in the comments below.

(defun 5-1-sequence (limit)
  (cons 1 (loop :for f := t :then (not f)
                :for n := 5 :then (* (if f 5 2) n)
                :while (< n limit)
                :collect n)))

(assert (equalp (5-1-sequence 100001)
                '(1 5 10 50 100 500 1000 5000 10000 50000 100000)))

(defun print-5-1-sequence (limit)
  (let ((*print-right-margin* 72))
    (pprint (5-1-sequence limit)))
  (values))

(print-5-1-sequence 10000001)

;; (1 5 10 50 100 500 1000 5000 10000 50000 100000 500000 1000000 5000000
;;  10000000)


(defun find-numbers (min max such-as)
  (loop
    :for i :from min :to max
    :when (funcall such-as i)
      :collect i))

(defun square (x) (* x x))

(assert (equalp (find-numbers 0 999
                              (lambda (n)
                                (let ((a (truncate n 100))
                                      (b (mod (truncate n 10) 10))
                                      (c (mod n 10)))
                                  (= (/ n 11)
                                     (+ (square a) (square b) (square c))))))
                '(0 550 803)))
