;; -*- mode:lisp;coding:utf-8 -*-
;;
;; https://programmingpraxis.com/2021/06/22/aronsons-sequence/#menu-item-2588
;;
;; Aronson’s Sequence
;; 
;; June 22, 2021
;; 
;; Aronson’s sequence 1, 4, 11, 16, 24, 29, 33, 35, 39, … (A005224) is an
;; infinite self-referential sequence defined as:
;; 
;;     T is the first, fourth, eleventh, … letter in this sentence.
;;    
;; Your task is to write a program that generates Aronson’s sequence and
;; use it to compute the first hundred members of the sequence. When you
;; are finished, you are welcome to read or run a suggested solution, or
;; to post your own solution or discuss the exercise in the comments
;; below.

(defun aronson (n)
  (let ((letters  (make-array 8192 :element-type 'character
                                   :fill-pointer 0 :adjustable t)))
    (format letters  "Tisthe")
    (loop
      :repeat n
      :for pos := (1+ (position #\t letters :test (function char-equal)))
        :then (1+ (position #\t letters :test (function char-equal)
                                        :start pos))
      :collect pos :into serie
      :do (format letters  "~:R"   pos)
      :finally (return (values serie (format nil "T is the ~{~:R~^, ~} letter in this sentence." serie))))))

(pprint (multiple-value-list (aronson 100)))

#|
((1 4 11 16 24 29 33 35 39 46 48 52 58 60 64 67 72 76 82 84 88 94 99 105 110 117 122 134 141 146 149 154 161 166 174
  181 195 201 207 220 227 233 240 246 253 259 265 282 296 299 318 322 336 340 361 365 372 389 396 412 418 435 441 458
  465 482 489 505 511 530 537 555 562 579 585 587 603 604 621 623 635 639 642 644 656 660 668 670 682 686 689 694 709
  712 714 729 735 737 752 755)
 "T is the first, fourth, eleventh, sixteenth, twenty-fourth, twenty-ninth, thirty-third, thirty-fifth, thirty-ninth, forty-sixth, forty-eighth, fifty-second, fifty-eighth, sixtieth, sixty-fourth, sixty-seventh, seventy-second, seventy-sixth, eighty-second, eighty-fourth, eighty-eighth, ninety-fourth, ninety-ninth, one hundred fifth, one hundred tenth, one hundred seventeenth, one hundred twenty-second, one hundred thirty-fourth, one hundred forty-first, one hundred forty-sixth, one hundred forty-ninth, one hundred fifty-fourth, one hundred sixty-first, one hundred sixty-sixth, one hundred seventy-fourth, one hundred eighty-first, one hundred ninety-fifth, two hundred first, two hundred seventh, two hundred twentieth, two hundred twenty-seventh, two hundred thirty-third, two hundred fortieth, two hundred forty-sixth, two hundred fifty-third, two hundred fifty-ninth, two hundred sixty-fifth, two hundred eighty-second, two hundred ninety-sixth, two hundred ninety-ninth, three hundred eighteenth, three hundred twenty-second, three hundred thirty-sixth, three hundred fortieth, three hundred sixty-first, three hundred sixty-fifth, three hundred seventy-second, three hundred eighty-ninth, three hundred ninety-sixth, four hundred twelfth, four hundred eighteenth, four hundred thirty-fifth, four hundred forty-first, four hundred fifty-eighth, four hundred sixty-fifth, four hundred eighty-second, four hundred eighty-ninth, five hundred fifth, five hundred eleventh, five hundred thirtieth, five hundred thirty-seventh, five hundred fifty-fifth, five hundred sixty-second, five hundred seventy-ninth, five hundred eighty-fifth, five hundred eighty-seventh, six hundred third, six hundred fourth, six hundred twenty-first, six hundred twenty-third, six hundred thirty-fifth, six hundred thirty-ninth, six hundred forty-second, six hundred forty-fourth, six hundred fifty-sixth, six hundred sixtieth, six hundred sixty-eighth, six hundred seventieth, six hundred eighty-second, six hundred eighty-sixth, six hundred eighty-ninth, six hundred ninety-fourth, seven hundred ninth, seven hundred twelfth, seven hundred fourteenth, seven hundred twenty-ninth, seven hundred thirty-fifth, seven hundred thirty-seventh, seven hundred fifty-second, seven hundred fifty-fifth letter in this sentence.")
|#
