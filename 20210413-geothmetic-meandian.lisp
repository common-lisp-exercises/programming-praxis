#|
https://ideone.com/Ukar9D
https://programmingpraxis.com/2021/04/13/geothmetic-meandian/

Geothmetic Meandian
April 13, 2021

In mathematics, the arithmetic geometric mean of two positive real
numbers is computed by repeatedly taking half their sum and the square
root of their product until the two numbers converge. For instance,
the arithmetic geometric mean of 24 and 6 is 13.456171…, with the
iterative steps computed as follows:

0  24                            6
1  15                           12
2  13.5                         13.416407864998738175455042
3  13.458203932499369089227521  13.458139030990984877207090
4  13.458171481745176983217305  13.458171481706053858316334
5  13.458171481725615420766820  13.458171481725615420766806

The arithmetic geometric mean was invented by Lagrange and studied by
Gauss, and is used today to compute various transcendental functions
because it converges so quickly.

In the world of Randall Munroe, the geothmetic meandian of any set of
positive numbers is computed by iterating three sequences — the
arithmetic mean, the geometric mean, and the median — until they
converge. For instance, the geothmetic meandian of the set (1,1,2,3,5)
is 2.089, computed as follows:

1  2.4                1.9743504858348200 2
2  2.1247834952782734 2.1161924605448084 2
3  2.0803253186076938 2.0795368194795802 2.1161924605448084
4  2.0920181995440275 2.0919486049152223 2.0803253186076938
5  2.0880973743556477 2.0880901331209600 2.0919486049152223
6  2.0893787041306098 2.0893779142184865 2.0880973743556477
7  2.0889513309015815 2.0889512436159920 2.0893779142184865
8  2.0890934962453533 2.0890934865653277 2.0889513309015815
9  2.0890461045707540 2.0890461034958396 2.0890934865653277

|#


(defun arithmetic-geometric-mean (a b)
  (values (/ (+ a b) 2)
          (sqrt (* a b))))

(defun converge (epsilon fun a b)
  (loop
    :while (< epsilon (abs (- a b)))
    :do (multiple-value-setq (a b) (funcall fun a b))
    :finally (return (values a b))))

(converge 1d-15
          (lambda (a b)
            (format t ";; ~24,21F ~24,21F~%" a b)
            (arithmetic-geometric-mean a b))
          24d0 6d0)

;; 24.000000000000000000000  6.000000000000000000000
;; 15.000000000000000000000 12.000000000000000000000
;; 13.500000000000000000000 13.416407864998739000000
;; 13.458203932499370000000 13.458139030990985000000
;; 13.458171481745177000000 13.458171481706055000000
;; --> 13.458171481725616D0
;;     13.458171481725616D0



(defun geothmetic-meandian (set)
  (sort (vector (/ (reduce (function +) set :initial-value 0.0d0)
                   (length set))
                (expt (reduce (function *) set  :initial-value 1.0d0)
                      (/ (length set)))
                (aref set (truncate (length set) 2)))
        (function <)))

(defun converge-set (epsilon fun set)
  ;; set is sorted.
  (loop
    :while (< epsilon (abs (- (aref set 0) (aref set (1- (length set))))))
    :do (setf set (funcall fun set))
    :finally (return set)))

(converge-set
  1d-3
  (lambda (set)
    (format t ";;~{ ~24,21F~}~%" (coerce set 'list))
    (geothmetic-meandian set))
  #(1 1 2 3 5))

;;  1.000000000000000000000  1.000000000000000000000  2.000000000000000000000  3.000000000000000000000  5.000000000000000000000
;;  1.974350485834820000000  2.000000000000000000000  2.400000000000000000000
;;  2.000000000000000000000  2.116192460544808400000  2.124783495278273000000
;;  2.079536819479580200000  2.080325318607693800000  2.116192460544808400000
;;  2.080325318607693800000  2.091948604915222300000  2.092018199544027500000
;;  2.088090133120960000000  2.088097374355647700000  2.091948604915222300000
;;  2.088097374355647700000  2.089377914218486500000  2.089378704130609800000
;; --> #(2.088951243615992D0 2.0889513309015815D0 2.0893779142184865D0)

