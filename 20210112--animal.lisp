;; ;; https://programmingpraxis.com/2021/01/12/animal-txt/
;; 
;; Animal.txt
;; January 12, 2021
;; Today’s task is from a beginning programmer, who starts with an input file called animal.txt:
;; 
;; There once was a Dog
;;  
;; Wednesday he ate Apples 
;; Thursday he ate Apples
;; Friday he ate Apples
;; Saturday he ate carrots
;;  
;; There once was a Bear
;;  
;; Tuesday he ate carrots
;; Wednesday he ate carrots
;; Thursday he ate chicken
;; 
;; 
;; 
;; He wants to create this output:
;; 
;; Food: Apples Animals who ate it: 1
;; =======
;; Dog
;;  
;; Food: Carrots Animals who ate it: 2
;; ========
;; Bear
;; Dog
;;  
;; Food: Chicken Animals who ate it: 1
;; ========
;; Bear


;; He gave a complicated awk solution that didn’t work; it produced
;; duplicate lines of output in those cases where the same animal ate the
;; same food on multiple days.
;; 
;; Your task is to write a program to produces the desired transformation
;; form input to output. When you are finished, you are welcome to read
;; or run a suggested solution, or to post your own solution or discuss
;; the exercise in the comments below.


;;;;-------------------------------------------------------------------
;;;
;;; 1- the animal file parser
;;;
;;; Note: it looks like the input is case insensitive (eg. "carrots"
;;; vs. "Carrots"), so we upcase all strings.

(defun read-next-non-empty-line (stream)
  (handler-case (loop
                  :for line := (string-trim #(#\space #\tab #\newline)
                                            (read-line stream))
                  :until (plusp (length line))
                  :finally (return line))
    (end-of-file () nil)))

(defun animal-parse-animal-declaration (lines)
  (let ((pos (and lines (mismatch "There once was a " (first lines)))))
    (list (when pos
            (string-upcase (subseq (pop lines) pos)))
          lines)))

(defun animal-parse-dish (lines)
  (let* ((separator " he ate ")
         (pos  (and lines (search separator (first lines))))
         (day  (and pos (string-upcase (subseq (first lines) 0 pos))))
         (food (and pos (string-upcase (subseq (first lines) (+ pos (length separator)))))))
    (list (when food
            (pop lines)
            (list day food))
          lines)))

(defun animal-parse-stream (stream)
  (loop
    :with eats  := '()
    :with lines := (loop :for line := (read-next-non-empty-line stream)
                         :while line
                         :collect line)
    :for (animal remaining-lines) := (animal-parse-animal-declaration lines)
    :while animal
    :do (setf lines remaining-lines)
        (loop
          :for ((day food) remaining-lines) := (animal-parse-dish lines)
          :while food
          :do (setf lines remaining-lines)
              (push (list animal day food) eats))
        :finally (return eats)))

(defun tests/animal-parse-stream ()
  (assert (equalp (with-input-from-string (stream "
There once was a Dog
 
Wednesday he ate Apples 
Thursday he ate Apples
Friday he ate Apples
Saturday he ate carrots
 
There once was a Bear
 
Tuesday he ate carrots
Wednesday he ate carrots
Thursday he ate chicken

")
                    (animal-parse-stream stream))
            '(("BEAR" "THURSDAY" "CHICKEN")
              ("BEAR" "WEDNESDAY" "CARROTS")
              ("BEAR" "TUESDAY" "CARROTS")
              ("DOG" "SATURDAY" "CARROTS")
              ("DOG" "FRIDAY" "APPLES")
              ("DOG" "THURSDAY" "APPLES")
              ("DOG" "WEDNESDAY" "APPLES"))))
  :success)


;;;
;;; 2- Process the data
;;;
;;; Note: the results seem to be sorted by food, and by animal name.

(defun foods-for-animals (eats)
  (loop :with foods := (make-hash-table :test (function equalp))
        :for (animal nil food) :in eats
        :do (pushnew animal (gethash food foods '())
                     :test (function string=))
        :finally (return (let ((result '()))
                           (maphash (lambda (k v)
                                      (push (list k (length v)
                                                  (sort v (function string-lessp)))
                                            result))
                                    foods)
                           (sort  result
                                  (function string-lessp)
                                  :key (function first))))))

;;;
;;; 3- Format the results.
;;;

(defun format-foods (foods)
  (format t "~:{Food: ~:(~A~) Animals who ate it: ~D~%=======~%~{~:(~A~)~%~}~%~}"
          foods))


;;;
;;; 4- wrap it up:
;;;

(defun main (input-file)
  (format-foods
   (foods-for-animals
          (with-open-file (stream input-file)
            (animal-parse-stream stream))))
  (values))

#|
(main #P"~/src/public/common-lisp-exercises/programming-praxis/animals.txt")

Food: Apples Animals who ate it: 1
=======
Dog

Food: Carrots Animals who ate it: 2
=======
Bear
Dog

Food: Chicken Animals who ate it: 1
=======
Bear

|#
