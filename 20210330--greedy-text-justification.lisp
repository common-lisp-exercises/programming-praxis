#|
https://programmingpraxis.com/2021/03/30/greedy-text-justification/

Greedy Text Justification

March 30, 2021

Today’s exercise would make a good interview question, though I don’t
know of anyone doing that; you should answer as if you are standing at
a whiteboard explaining your code as you go:

Given a string and a line width, split the string into words (a
maximal run of characters excluding spaces) and write the words onto
successive lines with spaces added between the words so that each line
is the requested width. Words should be added to lines greedily (as
many words as will fit) and extra spaces should be assigned to the
left of the output string. The last line should not have spaces added,
so it may be shorter than the other lines.

For example, the string “This is an example of text justification” is
written with a line width of 16 like this:

    ----+----+----+-
    This    is    an
    example  of text
    justification.
    ----+----+----+-

Your task is to write a program that greedily justifies text. When you
are finished, you are welcome to read or run a suggested solution, or
to post your own solution or discuss the exercise in the comments
below.

|#


(defun split-string (string &optional (separators " ") (remove-empty nil))
  "
STRING:         A sequence.

SEPARATOR:      A sequence.

RETURN:         A list of subsequence of STRING, split upon any element of SEPARATORS.
                Separators are compared to elements of the STRING with EQL.

NOTE:           It's actually a simple split-sequence now.

EXAMPLES:       (split-string '(1 2 0 3 4 5 0 6 7 8 0 9) '(0))
                --> ((1 2) (3 4 5) (6 7 8) (9))
                (split-string #(1 2 0 3 4 5 0 6 7 8 0 9) #(0))
                --> (#(1 2) #(3 4 5) #(6 7 8) #(9))
                (split-string \"1 2 0 3 4 5 0 6 7 8\" '(#\space #\0))
                --> (\"1\" \"2\" \"\" \"\" \"3\" \"4\" \"5\" \"\" \"\" \"6\" \"7\" \"8\")
"
  (loop
    :with strlen = (length string)
    :for position = 0 :then (1+ nextpos)
    :for nextpos = (position-if (lambda (e) (find e separators)) string :start position)
    :unless (and remove-empty
                 (or (and (= position strlen) (null nextpos))
                     (eql position nextpos)))
    :collect (subseq string position nextpos)
    :while nextpos))

(defun string-justify (string &optional (width 72) (left-margin 0)
                                (separators #(#\Space #\Newline))
                                (filler #\space)
                                (justification :left))
  "
RETURN:         A left-justified string built from string.

WIDTH:          The maximum width of the generated lines.  Default is 72 characters.

LEFT-MARGIN:    The left margin, filled with spaces.  Default is 0 characters.

SEPARATORS:     A sequence containing the characters on which to split the words.
                Default: #\(#\space #\newline).

FILLER:         The character used as space to fill between words.

JUSTIFICATION   :left, :right, :center or :full
"
  (check-type string string)
  (check-type width integer)
  (check-type left-margin integer)
  (check-type filler character)
  (check-type justification (member :left :right :center :full))

  (flet ((justify (line width stream)
           ;; Justifies a single line
           (let* ((nwords      (length line))
                  (line-length (reduce (function +) line
                                       :initial-value (- nwords -1)
                                       :key (function length))))
             (case justification
               (:left
                (write-string (pop line) stream)
                (dolist (word line))
                (write-string line stream)
                (write-string (make-string (- width (length line)) :initial-element fill) stream))
               (:right
                (write-string (make-string (- width (length line)) :initial-element fill) stream)
                (write-string line stream))
               (:center
                 (let ((left (truncate (- width (length line)) 2)))
                   (write-string (make-string left :initial-element fill) stream)
                   (write-string line stream)
                   (write-string (make-string (- width (length line) left) :initial-element fill) stream)))
               (:full
                (let* ((words     (split-sequence #\space line))
                       (increment (if (rest words)
                                      (/ (- width (length line)) (1- (length words)))
                                      0))
                       (spaces    0))
                  (write-string (first words) stream)
                  (dolist (word (rest words))
                    (setf spaces (+ spaces 1 increment))
                    (write-string (make-string (truncate spaces) :initial-element fill) stream)
                    (setf spaces (nth-value 1 (truncate spaces)))
                    (write-string word stream))))))))
  
    (let* ((margin    (make-string left-margin :initial-element (character " ")))
           (splited   (split-string string separators t))
           (col       left-margin)
           (justified (list (subseq margin 0 col)))
           (separator ""))
      (dolist (word splited)
        (if (<= width (+ col (length word)))
            (progn (push #(#\newline) justified)
                   (push margin justified)
                   (push word justified)
                   (setf col (+ left-margin (length word))))
            (progn (push separator justified)
                   (push word justified)
                   (incf col (+ 1 (length word)))))nn
        (setf separator " "))
      ;; ;; Pad with spaces up to width.
      ;; (when (< col width)
      ;;   (push (make-string (- width col) :initial-element (character " "))
      ;;         justified))
      (apply (function concatenate) 'string (nreverse justified)))))
