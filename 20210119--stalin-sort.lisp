;; https://programmingpraxis.com/2021/01/19/stalin-sort/

;; Stalin Sort
;; January 19, 2021
;;
;; Stalin Sort is a single-pass sort that operates in O(1) space and O(n)
;; time. Iterate down the list of elements checking if they are in
;; order. Any element which is out of order is sent to the gulag
;; (eliminated from the list). At the end you have a sorted list, though
;; it may not be a permutation of the original list. As an example, the
;; list (1 2 5 3 5 7) is Stalin-sorted as (1 2 5 5 7); the 3 is dropped
;; because it is less than the 5 that it follows.



;; First, a functional solution, without mutation.
;; It's not the most optimized way to do it, since key is called twice per element,
;; and it' returns a new list even when the list is already sorted.
;; Also, since it's not a terminal-recursive function, the length of
;; list is strongly limited by the size of the stack.

(defun stalin-sort-list (list lessp &key (key (function identity)))
  (when list
    (let* ((current (first list))
           (kurrent (funcall key current)))
      (if (null (rest list))
          (list current)
          (let* ((next (second list))
                 (kext (funcall key next)))
            (if (funcall lessp kurrent kext)
                (cons current (stalin-sort-list (rest list) lessp :key key))
                (stalin-sort-list (cons current (rest (rest list)))  lessp :key key)))))))

(assert (equal (stalin-sort-list '(1 2 5 3 5 7) (function <))
               '(1 2 5 7)))

(assert (equal (stalin-sort-list '(1 2 5 3 5 7) (function <=))
               '(1 2 5 5 7)))


;; Here is a destructive version.
;; The list itself is modified to remove the unsorted elements.
;; We also collect the removed elements in the cells as second value.

(defun nstalin-sort-list (list lessp &key (key (function identity)))
  (when list
    (loop
      :with gulag        := (cons nil nil)
      :with gate         := gulag
      :with current-cell := list
      :with key-current  := (funcall key (car current-cell))
      :with next-cell    := (cdr current-cell)
      :with key-next
      :while next-cell
      :do (setf key-next (funcall key (car next-cell)))
          (if (funcall lessp key-current key-next)
              (setf current-cell next-cell
                    next-cell (cdr current-cell)
                    key-current key-next)
              (setf (cdr current-cell) (cdr next-cell)
                    (cdr gate) next-cell
                    gate (cdr gate)
                    (cdr gate) nil
                    next-cell (cdr current-cell)))
      :finally (return (values list (cdr gulag))))))

(let* ((list '(1 2 5 3 5 7))
       (mutable-list (copy-list list)))
  (multiple-value-bind (sorted gulag) (nstalin-sort-list mutable-list (function <))
    (assert (eq mutable-list sorted))
    (assert (= (+ (length sorted) (length gulag)) (length list)))
    (assert (equal sorted '(1 2 5 7)))))

(let* ((list '(1 2 5 3 5 7))
       (mutable-list (copy-list list)))
  (multiple-value-bind (sorted gulag) (nstalin-sort-list mutable-list (function <=))
    (assert (eq mutable-list sorted))
    (assert (= (+ (length sorted) (length gulag)) (length list)))
    (assert (equal sorted '(1 2 5 5 7)))))

(let* ((list1 (loop repeat 100000 collect (random 10000000)))
       (list2 (copy-list list1))
       (sorted-list1 (time (stalin-sort-list list1 (function <))))
       (sorted-list2 (time (nstalin-sort-list list2 (function <)))))
  (assert (equal sorted-list1 sorted-list2)))
